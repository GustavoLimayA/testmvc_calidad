﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using TestMVC;
using TestMVC.Service;

namespace MVCTestin.IPersonaServeceFake
{
    public class PersonaServeceFake : IPersonaService
    {
        public Persona GetPersona(int id)
        {
            return new Persona();
        }

        public List<Persona> GetPersonas()
        {
            return new List<Persona>
            {
                new Persona(),
                new Persona(),
            };
        }
    }
}