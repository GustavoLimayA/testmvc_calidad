﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using NUnit.Framework;
using TestMVC.Controllers;
using System.Web.Mvc;
using TestMVC;
using MVCTestin.IPersonaServeceFake;

namespace MVCTestin
{
    [TestFixture]
    public class Class1
    {
        [Test]
        public void Test_1()
        {
            var controllerHome = new HomeController(new PersonaServeceFake());
            var result = (ViewResult)controllerHome.Index();
            Assert.IsInstanceOf<ViewResult>(result);
            Assert.IsInstanceOf<List<Persona>>(result.Model);
            Assert.IsNotNull(result.Model);
        }

        [Test]
        public void Test_2()
        {
            var controllerHome = new HomeController(new PersonaServeceFake());
            var result = (ViewResult)controllerHome.Show(0);
            Assert.IsInstanceOf<ViewResult>(result);
            Assert.IsInstanceOf<Persona>(result.Model);
            Assert.IsNotNull(result.Model);
        }

        [Test]
        public void Test_3()
        {
            var controllerHome = new HomeController(new PersonaServeceFake());
            var result = (ViewResult)controllerHome.Index();
            Assert.AreEqual(2, (result.Model as List<Persona>).Count);
        }
    }
}