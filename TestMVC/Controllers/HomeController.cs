﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using TestMVC.Service;

namespace TestMVC.Controllers
{
    public class HomeController : Controller
    {
        private IPersonaService service;

        public HomeController(IPersonaService service)
        {
            this.service = service;
        }

        public ActionResult Index()
        {
            var lista = service.GetPersonas();
            return View(lista);
        }

        public ActionResult Show(int id)
        {
            return View(service.GetPersona(id));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }
    }
}