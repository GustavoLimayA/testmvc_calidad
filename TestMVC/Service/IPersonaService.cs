﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace TestMVC.Service
{
    public interface IPersonaService
    {
        List<Persona> GetPersonas();

        Persona GetPersona(int id);
    }
}